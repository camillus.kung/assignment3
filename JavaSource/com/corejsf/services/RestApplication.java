package com.corejsf.services;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Rest application class that stores the rest resources.
 * @author camilluskung
 * @version 1.0
 */
@ApplicationPath("/services")
public class RestApplication extends Application {

    /** 
     * Set of resource classes.
     */
    private final Set<Class<?>> classes;
    
    /**
     * Constructor that adds all resource classes to a hashset.
     */
    public RestApplication() {
        HashSet<Class<?>> c = new HashSet<>();
        c.add(UserResources.class);
        c.add(TimesheetResources.class);
        c.add(AuthenticationResources.class);
        classes = Collections.unmodifiableSet(c);
    }

    @Override
    public Set<Class<?>> getClasses() {
        return classes;
    }
    
}
