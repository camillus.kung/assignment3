package com.corejsf.services;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.corejsf.access.TokenManager;
import com.corejsf.access.UserManager;
import com.corejsf.info.EmployeeSession;
import com.corejsf.model.Token;
import com.corejsf.model.User;

/**
 * Resource for user logging in and out.
 * @author camilluskung
 * @version 1.0
 *
 */
@RequestScoped
@Path("/authenticate")
public class AuthenticationResources {
    
    /**
     * Bean for database access to create, update, read and delete 
     * from the User table.
     */
    @Inject private UserManager userManager = new UserManager();
    
    /**
     * Bean for generating and removing tokens/sessions.
     */
    @Inject private EmployeeSession session = new EmployeeSession();
    
    /**
     * Bean for database access to create, update, read and delete 
     * from the Tokens table.
     */
    @Inject private TokenManager tokenManager = new TokenManager();

    /**
     * Logs user in.
     * @param username the username of user
     * @param password the password of user
     * @return Response with token info
     */
    @POST
    @Path("login")
    @Consumes("application/x-www-form-urlencoded")
    @Produces(MediaType.APPLICATION_JSON)
    public Response login(@FormParam("username") String username,
            @FormParam("password") String password) {
        session.removeTimedoutTokens();
        
        if (username == null || password == null) {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        
        User user = userManager.verify(username, password);
        
        if (user == null) {
            throw new WebApplicationException(Response.Status.UNAUTHORIZED); 
        }
        
        Token token = session.generateToken(user.getEmployeeId()); 
        return Response.ok(token).build();
        
    }
    
    /**
     * Logs user out.
     * @param tokenId the token id used to authenticate requester
     * @return Response with no content
     */
    @DELETE
    @Path("logout")
    @Produces(MediaType.APPLICATION_JSON)
    public Response logout(@HeaderParam("tokenId") String tokenId) {
        session.removeTimedoutTokens();
        Token token = tokenManager.getTokenById(tokenId);
        
        if (token == null) {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        }
        
        session.removeToken(tokenId);
        
        return Response.noContent().build();
    }
    
}
