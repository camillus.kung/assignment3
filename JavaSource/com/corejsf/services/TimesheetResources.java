package com.corejsf.services;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.corejsf.access.TimesheetManager;
import com.corejsf.model.TimesheetDetail;
import com.corejsf.model.TimesheetHours;

import ca.bcit.infosys.timesheet.Timesheet;
import ca.bcit.infosys.timesheet.TimesheetRow;

/**
 * Resource for CRUD operations for Timesheets.
 * @author Rei
 * @version 1.0
 *
 */
@RequestScoped
@Path("/timesheet")
public class TimesheetResources {

    /**
     * Bean for database access to create, update, read and delete 
     * from the Timesheet table.
     */
    @Inject private TimesheetManager timesheetManager = new TimesheetManager();
    
    /**
     * Retrieves an employee's Timesheet.
     * @param employeeId the employee id of the user
     * @param weekNo the week number
     * @return Response with Timesheet as JSON
     */
    @POST
    @Path("/retrieveTimesheet")
    @Consumes("application/x-www-form-urlencoded")
    @Produces(MediaType.APPLICATION_JSON)
    public Response retrieveTimesheet(
            @FormParam("employeeId") int employeeId,
            @FormParam("weekNo") int weekNo) {
        Timesheet timesheet = null;
        
        try {
            timesheet = 
                    timesheetManager.getTimesheetByEmployeeAndWeekNo(
                    employeeId, weekNo);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        return Response.ok(timesheet).build();
    }
    
    /**
     * Adds a Timesheet to the database.
     * @param timesheetDetail the Timesheet to add
     * @return Response with Timesheet
     */
    @POST
    @Path("/createTimesheet")
    @Consumes(MediaType.APPLICATION_XML)
    public Response createTimesheet(TimesheetDetail timesheetDetail) {
        Calendar c = new GregorianCalendar();
        Timesheet timesheet = new Timesheet();
        ArrayList<TimesheetRow> details = new ArrayList<TimesheetRow>();
        
        int employeeId = timesheetDetail.getEmpId();
        int weekNumber = timesheetDetail.getWeekNo();
        
        timesheet.setEmployee(timesheetManager.getEmployeeByEmpId(employeeId));
        timesheet.setWeekNumber(weekNumber, c.get(Calendar.YEAR));
        
        List<TimesheetHours> weeklyHours = timesheetDetail.getWeeklyHours();
        
        for (int i = 0; i < weeklyHours.size(); i++) {
            int id = Integer.valueOf(weeklyHours.get(i).getProjectId());
            String workPackage = weeklyHours.get(i).getWorkPackage();
            BigDecimal[] hours = {
                    BigDecimal.valueOf(weeklyHours.get(i).getSat()),
                    BigDecimal.valueOf(weeklyHours.get(i).getSun()),
                    BigDecimal.valueOf(weeklyHours.get(i).getMon()),
                    BigDecimal.valueOf(weeklyHours.get(i).getTue()),
                    BigDecimal.valueOf(weeklyHours.get(i).getWed()),
                    BigDecimal.valueOf(weeklyHours.get(i).getThu()),
                    BigDecimal.valueOf(weeklyHours.get(i).getFri()),
            };
            String notes = weeklyHours.get(i).getNotes();
            details.add(new TimesheetRow(id, workPackage, hours, notes));
        }
        
        timesheet.setDetails(details);
     
        timesheetManager.addTimesheet(timesheet);
        
        return Response.ok().build();
    }
    
    /**
     * Updates a Timesheet in the database.
     * @param timesheetDetail the Timesheet to update
     * @return Response with Timesheet
     */
    @POST
    @Path("/updateTimesheet")
    @Consumes(MediaType.APPLICATION_XML)
    public Response updateTimesheet(TimesheetDetail timesheetDetail) {
        Calendar c = new GregorianCalendar();
        Timesheet timesheet = new Timesheet();
        ArrayList<TimesheetRow> details = new ArrayList<TimesheetRow>();
        
        int employeeId = timesheetDetail.getEmpId();
        int weekNumber = timesheetDetail.getWeekNo();
        
        timesheet.setEmployee(timesheetManager.getEmployeeByEmpId(employeeId));
        timesheet.setWeekNumber(weekNumber, c.get(Calendar.YEAR));
        
        List<TimesheetHours> weeklyHours = timesheetDetail.getWeeklyHours();
        
        for (int i = 0; i < weeklyHours.size(); i++) {
            int id = Integer.valueOf(weeklyHours.get(i).getProjectId());
            String workPackage = weeklyHours.get(i).getWorkPackage();
            BigDecimal[] hours = {
                    BigDecimal.valueOf(weeklyHours.get(i).getSat()),
                    BigDecimal.valueOf(weeklyHours.get(i).getSun()),
                    BigDecimal.valueOf(weeklyHours.get(i).getMon()),
                    BigDecimal.valueOf(weeklyHours.get(i).getTue()),
                    BigDecimal.valueOf(weeklyHours.get(i).getWed()),
                    BigDecimal.valueOf(weeklyHours.get(i).getThu()),
                    BigDecimal.valueOf(weeklyHours.get(i).getFri()),
            };
            String notes = weeklyHours.get(i).getNotes();
            details.add(new TimesheetRow(id, workPackage, hours, notes));
        }
        
        timesheet.setDetails(details);
     
        timesheetManager.updateTimesheet(timesheet);
        
        return Response.ok().build();
    }
}
