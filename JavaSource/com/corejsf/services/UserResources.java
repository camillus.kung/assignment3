package com.corejsf.services;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.corejsf.access.TokenManager;
import com.corejsf.access.UserManager;
import com.corejsf.info.EmployeeSession;
import com.corejsf.model.Token;
import com.corejsf.model.User;


import java.util.ArrayList;

/**
 * Resource for CRUD operations for users that are authenticated.
 * @author camilluskung
 * @version 1.0
 *
 */
@RequestScoped
@Path("/users")
public class UserResources {
    
    /**
     * Constant user id for super user.
     */
    private static final int SUPER_USER_ID = 0;
    
    /**
     * Constant for minimum employee id.
     */
    private static final int MIN_ID = 1;
    
    /**
     * Constant for maximum employee id.
     */
    private static final int MAX_ID = 9999;
    
    /**
     * Bean for database access to create, update, read and delete 
     * from the User table.
     */
    @Inject private UserManager userManager = new UserManager();
    
    /**
     * Bean for database access to create, update, read and delete 
     * from the Tokens table.
     */
    @Inject private TokenManager tokenManager = new TokenManager();
    
    /**
     * Bean for generating and removing tokens/sessions.
     */
    @Inject private EmployeeSession session = new EmployeeSession();
    
    
    /**
     * Retrieves a user.
     * @param tokenId the token id used to authenticate requester
     * @param employeeId the employee id of user
     * @return Response with user as JSON
     */
    @GET
    @Path("/user/{employeeId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUser(@HeaderParam("tokenId") String tokenId,
            @PathParam("employeeId") int employeeId)  {
        session.removeTimedoutTokens();
        Token token = tokenManager.getTokenById(tokenId);
        User user = userManager.getUserById(employeeId);
        
        if (token == null || token.getEmployeeId() != SUPER_USER_ID) {
            throw new WebApplicationException(Response.Status.FORBIDDEN); 
        } else if (user == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        
        return Response.ok(user).build();
    } 

    /**
     * Retrieves all users.
     * @param tokenId the token id used to authenticate requester
     * @return Response with list of user as JSON
     */
    @GET
    @Path("/userList")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUserList(@HeaderParam("tokenId") String tokenId)  {
        session.removeTimedoutTokens();
        Token token = tokenManager.getTokenById(tokenId);
        
        if (token == null || token.getEmployeeId() != SUPER_USER_ID) {
            throw new WebApplicationException(Response.Status.FORBIDDEN); 
        }
        
        ArrayList<User> list = userManager.getUserList();
        return Response.ok(list).build();
    } 

    /**
     * Adds a user.
     * @param tokenId the token id used to authenticate requester
     * @param employeeId the employee id of user
     * @param firstName the first name of user
     * @param lastName the last name of user
     * @param username the user name of user
     * @param password the password of user
     * @param confirmPassword the confirmed password of user
     * @return Response with added user as JSON
     */
    @POST
    @Path("/addUser")
    @Consumes("application/x-www-form-urlencoded")
    @Produces(MediaType.APPLICATION_JSON)
    public Response addUser(@HeaderParam("tokenId") String tokenId,
            @FormParam("employeeId") int employeeId,
            @FormParam("firstName") String firstName,
            @FormParam("lastName") String lastName,
            @FormParam("username") String username,
            @FormParam("password") String password,
            @FormParam("confirmPassword") String confirmPassword) {
        session.removeTimedoutTokens();
        Token token = tokenManager.getTokenById(tokenId);
        
        if (token == null || token.getEmployeeId() != SUPER_USER_ID) {
            throw new WebApplicationException(Response.Status.FORBIDDEN); 
        } else if (!password.equals(confirmPassword) 
                || firstName == null || lastName == null || username == null 
                || password == null || confirmPassword == null
                || employeeId < MIN_ID || employeeId > MAX_ID) {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        } else if (userManager.userNumberOrUsernameExists(employeeId,
                username)) {
            throw new WebApplicationException(Response.Status.CONFLICT);
        }
        
        User user = new User(employeeId, firstName, lastName, username, 
                password);
        userManager.addUser(user);
        return Response.ok(user).build();
    }
    
    /**
     * Deletes a user.
     * @param tokenId the token id used to authenticate requester.
     * @param employeeId the employee id of user
     * @return Response with deleted user as JSON
     */
    @DELETE
    @Path("/deleteUser/{employeeId}")
    @Consumes("application/x-www-form-urlencoded")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteUser(@HeaderParam("tokenId") String tokenId,
            @PathParam("employeeId") int employeeId) {
        session.removeTimedoutTokens();
        Token token = tokenManager.getTokenById(tokenId);
        User user = userManager.getUserById(employeeId);
        
        if (token == null || token.getEmployeeId() != SUPER_USER_ID 
                || employeeId == SUPER_USER_ID) {
            throw new WebApplicationException(Response.Status.FORBIDDEN); 
        } else if (user == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        
        userManager.deleteUser(user);
        return Response.ok(user).build();
    }
    
    /**
     * Updates a user.
     * @param tokenId the token id used to authenticate requester
     * @param employeeId the employee id of user
     * @param firstName the first name of user
     * @param lastName the last name of user
     * @param username the user name of user
     * @param password the password of user
     * @return Response with updated user as JSON
     */
    @PUT
    @Path("/update/{employeeId}")
    @Consumes("application/x-www-form-urlencoded")
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateUser(@HeaderParam("tokenId") String tokenId,
            @PathParam("employeeId") int employeeId,
            @FormParam("firstName") String firstName,
            @FormParam("lastName") String lastName,
            @FormParam("username") String username,
            @FormParam("password") String password) {
        session.removeTimedoutTokens();
        Token token = tokenManager.getTokenById(tokenId);
        
        User userToUpdate = userManager.getUserById(employeeId);
        
        if (token == null || token.getEmployeeId() != SUPER_USER_ID 
                || employeeId == SUPER_USER_ID) {
            throw new WebApplicationException(Response.Status.FORBIDDEN);
        } else if (employeeId < MIN_ID || employeeId > MAX_ID) {
            throw new WebApplicationException(Response.Status.BAD_REQUEST);
        } else if (userToUpdate == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        
        if (firstName != null) {
            userToUpdate.setFirstName(firstName);
        }
        
        if (lastName != null) {
            userToUpdate.setLastName(lastName);
        }
        
        if (username != null) {
            userToUpdate.setUserName(username);
        }
        
        if (password != null) {
            userToUpdate.setPassword(password);
        }
        
        userManager.updateUser(userToUpdate);
        
        return Response.ok(userToUpdate).build();
    }
}
