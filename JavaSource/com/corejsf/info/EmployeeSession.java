package com.corejsf.info;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import com.corejsf.access.TokenManager;
import com.corejsf.model.Token;

/**
 * Class generates sessions, removes session and removes timed out sessions.
 * @author camilluskung
 * @version 1.0
 *
 */
@ApplicationScoped
public class EmployeeSession implements Serializable {

    /**
     * Constant for an hour in milliseconds.
     */
    private static final long HOUR = 3600000;
    
    /**
     * Bean for database access to create, update, read and delete 
     * from the Tokens table.
     */
    @Inject private TokenManager tokenManager = new TokenManager();
    
    /**
     * The current token.
     */
    private Token currentToken;
    
    /**
     * Generates the token in the Tokens table.
     * @param employeeId the employee id of user
     * @return token
     */
    public Token generateToken(int employeeId) {
        Date date = new Date();
        long time = date.getTime();
        String uuid = UUID.randomUUID().toString();
        currentToken = new Token(uuid, employeeId, time);
        tokenManager.addToken(currentToken);
        
        return currentToken;
    }
    
    /**
     * Removes token from Tokens table.
     * @param tokenId the token id of session
     * @return true if removed, false otherwise
     */
    public boolean removeToken(String tokenId) {
        if (!tokenManager.removeToken(tokenId)) {
            return false;
        }
        
        return true;
    }
    
    /**
     * Removes all timed out tokens. Tokens expires after an hour.
     */
    public void removeTimedoutTokens() {
        Date date = new Date();
        long time = date.getTime();
        
        ArrayList<Token> tokenList = tokenManager.getTokenList();
        
        for (Token token : tokenList) {
            long timeDiff = time - token.getTimeout();
            
            if (timeDiff >= HOUR) {
                tokenManager.removeToken(token.getTokenId());
            }
        }
    }
    
}
