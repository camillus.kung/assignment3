package com.corejsf.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * POJO representing a Timesheet.
 * @author Rei
 * @version 1.0
 *
 */
@XmlRootElement(name = "timesheet")
public class TimesheetDetail {

    /** Employee ID. */
    private int empId;
    
    /** Week number. */
    private int weekNo;
    
    /** List of weekly hours. */
    private List<TimesheetHours> weeklyHours;
    
    /**
     * Getter for employee id.
     * @return employee id
     */
    @XmlElement(name = "empId")
    public int getEmpId() {
        return empId;
    }

    /**
     * Setter for employee id.
     * @param empId employee id
     */
    public void setEmpId(int empId) {
        this.empId = empId;
    }
    
    /**
     * Getter for week number.
     * @return week number
     */
    @XmlElement(name = "weekNo")
    public int getWeekNo() {
        return weekNo;
    }

    /**
     * Setter for week number.
     * @param weekNo week number
     */
    public void setWeekNo(int weekNo) {
        this.weekNo = weekNo;
    }
    
    /**
     * Getter weekly hours.
     * @return Weekly hours
     */
    @XmlElementWrapper(name = "weeklyHours")
    @XmlElement(name = "timesheetRow")
    public List<TimesheetHours> getWeeklyHours() {
        return weeklyHours;
    }

    /**
     * Setter for weekly hours.
     * @param weeklyHours weekly hours
     */
    public void setWeeklyHours(List<TimesheetHours> weeklyHours) {
        this.weeklyHours = weeklyHours;
    }
}
