package com.corejsf.model;

import java.io.Serializable;

import javax.faces.view.ViewScoped;

/**
 * A bean representing the token.
 * @author camilluskung
 * @version 1.0
 *
 */
@ViewScoped
public class Token implements Serializable {
    
    /**
     * The token id of session.
     */
    private String tokenId;
    
    /**
     * The employee id of user.
     */
    private int employeeId;
    
    /**
     * The time the session was logged.
     */
    private long timeout;
    
    /**
     * Empty Constructor.
     */
    public Token() {
        
    }
    
    /**
     * Constructor of token.
     * @param tokenId the token id of session
     * @param employeeId the employee id of user
     * @param timeout the time the session was logged
     */
    public Token(String tokenId, int employeeId, long timeout) {
        this.tokenId = tokenId;
        this.employeeId = employeeId;
        this.timeout = timeout;
    }
    
    /**
     * Getter for token id.
     * @return token id of session
     */
    public String getTokenId() {
        return tokenId;
    }
    /**
     * Setter for token id.
     * @param tokenId the token id of session
     */
    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }
    
    /**
     * Getter for employee id.
     * @return employee id of user
     */
    public int getEmployeeId() {
        return employeeId;
    }
    
    /**
     * Sette for employee id.
     * @param employeeId the employee id of user
     */
    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }
    
    /**
     * Getter for time session was logged.
     * @return time session was logged
     */
    public long getTimeout() {
        return timeout;
    }
    
    /**
     * Setter for time session was logged.
     * @param timeout the time session was logged
     */
    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }

}
