package com.corejsf.model;

import java.io.Serializable;

import javax.faces.view.ViewScoped;
import javax.inject.Named;

import ca.bcit.infosys.employee.Credentials;

/**
 * Bean representing user credentials.
 * @author camilluskung
 * @version 1.0
 *
 */
@Named("userCred")
@ViewScoped
public class UserCredentials extends Credentials implements Serializable {

    /**
     * New password of user.
     */
    private String newPassword;
    
    /**
     * Confirmation password of user.
     */
    private String confirmPassword;
    
    /**
     * New user name of user.
     */
    private String newUserName;

    /**
     * Getter for password confirmation.
     * @return confirmed password
     */
    public String getConfirmPassword() {
        return confirmPassword;
    }

    /**
     * Setter for password confirmation.
     * @param confirmPassword the confirmation password
     */
    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    /**
     * Getter for new password.
     * @return new password
     */
    public String getNewPassword() {
        return newPassword;
    }

    /**
     * Setter for a new password.
     * @param newPassword the new password of user
     */
    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    /**
     * Getter for a new user name.
     * @return new user name
     */
    public String getNewUserName() {
        return newUserName;
    }

    /**
     * Setter for a new user name.
     * @param newUserName the new user name
     */
    public void setNewUserName(String newUserName) {
        this.newUserName = newUserName;
    }
    
    
}
