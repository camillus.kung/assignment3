package com.corejsf.model;

import java.io.Serializable;

import javax.faces.view.ViewScoped;

/**
 * A bean representing the user.
 * @author camilluskung
 * @version 1.0
 */
@ViewScoped
public class User implements Serializable {

    /**
     * Employee id of employee.
     */
    private int employeeId;
    
    /**
     * Username of employee.
     */
    private String userName;
    
    /**
     * First name of employee.
     */
    private String firstName;
    
    /**
     * Last name of employee.
     */
    private String lastName;
    
    /**
     * Password of the user.
     */
    private String password;
    
    /**
     * Empty constructor.
     */
    public User() {
        
    }
    
    /**
     * Constructor of a user.
     * @param employeeId the employee id of the user
     * @param firstName the first name of user
     * @param lastName the last name of user
     * @param userName the user name of the user
     * @param password the password of the user
     */
    public User(int employeeId, String firstName, String lastName, 
            String userName, String password) {
        this.employeeId = employeeId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.userName = userName;
        this.password = password;
    }
    
    /**
     * Constructor of a user.
    * @param firstName the first name of user
     * @param lastName the last name of user
     * @param userName the user name of the user
     * @param password the password of the user
     */
    public User(String firstName, String lastName, String userName, 
            String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.userName = userName;
        this.password = password;
    }
    
    /**
     * Getter for the password.
     * @return password of user
     */
    public String getPassword() {
        return password;
    }

    /**
     * Setter for the password.
     * @param password the password of the user
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Getter for first name.
     * @return first name of user
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Setter for first name.
     * @param firstName the first name of the user
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Getter for last name.
     * @return last name of user
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Setter for last name.
     * @param lastName the last name of user
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Getter for username.
     * @return username of user
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Setter for username.
     * @param userName the username of user
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * Getter for employee id.
     * @return employee id of user
     */
    public int getEmployeeId() {
        return employeeId;
    }

    /**
     * Setter for employee id.
     * @param employeeId the employee id of user
     */
    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

}
