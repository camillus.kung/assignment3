package com.corejsf.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * POJO representing a TimesheetRow.
 * @author Rei
 * @version 1.0
 *
 */
@XmlRootElement(name = "timesheetRow")
public class TimesheetHours {
    
    /** Project ID. */
    private int projectId;
    
    /** Work Package. */
    private String workPackage;
    
    /** Hours for Saturday. */
    private double sat;
    
    /** Hours for Sunday. */
    private double sun;
    
    /** Hours for Monday. */
    private double mon;
    
    /** Hours for Tuesday. */
    private double tue;
    
    /** Hours for Wednesday. */
    private double wed;
    
    /** Hours for Thursday. */
    private double thu;
    
    /** Hours for Friday. */
    private double fri;

    /** Notes. */
    private String notes;
    
    /**
     * Getter for project id.
     * @return Project id
     */
    @XmlElement(name = "projectId")
    public int getProjectId() {
        return projectId;
    }

    /**
     * Setter for project id.
     * @param projectId project id
     */
    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }
    
    /**
     * Getter for work package.
     * @return work package
     */
    @XmlElement(name = "workPackage")
    public String getWorkPackage() {
        return workPackage;
    }

    /**
     * Setter for work package.
     * @param workPackage work package
     */
    public void setWorkPackage(String workPackage) {
        this.workPackage = workPackage;
    }
    
    /**
     * Getter for hours for Saturday.
     * @return Saturday hours
     */
    @XmlElement(name = "sat")
    public double getSat() {
        return sat;
    }

    /**
     * Setter for hours for Saturday.
     * @param sat Saturday hours
     */
    public void setSat(double sat) {
        this.sat = sat;
    }

    /**
     * Getter for hours for Sunday.
     * @return Sunday hours
     */
    @XmlElement(name = "sun")
    public double getSun() {
        return sun;
    }

    /**
     * Setter for hours for Sunday.
     * @param sun Sunday hours
     */
    public void setSun(double sun) {
        this.sun = sun;
    }

    /**
     * Getter for hours for Monday.
     * @return Monday hours
     */
    @XmlElement(name = "mon")
    public double getMon() {
        return mon;
    }

    /**
     * Setter for hours for Monday.
     * @param mon Monday hours
     */
    public void setMon(double mon) {
        this.mon = mon;
    }

    /**
     * Getter for hours for Tuesday.
     * @return Tuesday hours
     */
    @XmlElement(name = "tue")
    public double getTue() {
        return tue;
    }

    /**
     * Setter for hours for Tuesday.
     * @param tue Tuesday hours
     */
    public void setTue(double tue) {
        this.tue = tue;
    }

    /**
     * Getter for hours for Wednesday.
     * @return Wednesday hours
     */
    @XmlElement(name = "wed")
    public double getWed() {
        return wed;
    }

    /**
     * Setter for hours for Wednesday.
     * @param wed Wednesday hours
     */
    public void setWed(double wed) {
        this.wed = wed;
    }

    /**
     * Getter for hours for Thursday.
     * @return Thursday hours
     */
    @XmlElement(name = "thu")
    public double getThu() {
        return thu;
    }

    /**
     * Setter for hours for Thursday.
     * @param thu Thursday hours
     */
    public void setThu(double thu) {
        this.thu = thu;
    }

    /**
     * Getter for hours for Friday.
     * @return Friday hours
     */
    @XmlElement(name = "fri")
    public double getFri() {
        return fri;
    }

    /**
     * Setter for hours for Friday.
     * @param fri Friday hours
     */
    public void setFri(double fri) {
        this.fri = fri;
    }
    
    
    /**
     * Getter for notes.
     * @return notes
     */
    @XmlElement(name = "notes")
    public String getNotes() {
        return notes;
    }

    /**
     * Setter for notes.
     * @param notes notes
     */
    public void setNotes(String notes) {
        this.notes = notes;
    }
}
