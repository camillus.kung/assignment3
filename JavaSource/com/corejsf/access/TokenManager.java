package com.corejsf.access;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.annotation.Resource;
import javax.enterprise.context.ConversationScoped;
import javax.sql.DataSource;

import com.corejsf.model.Token;

/**
 * Resources for tokens.
 * @author camilluskung
 * @version 1.0
 *
 */
@ConversationScoped
public class TokenManager implements Serializable {

    /** 
     * dataSource for connection pool on JBoss AS 7 or higher. 
     */
    @Resource(mappedName = "java:jboss/datasources/assignment3")
    private DataSource dataSource;
    
    /**
     * Adds token.
     * @param token the token of the session.
     * @return true if added successfully false otherwise
     */
    public boolean addToken(Token token) {
        //order of fields in INSERT statement
        final int tokenId = 1;
        final int employeeId = 2;
        final int timeout = 3;
        
        Connection connection = null;
        PreparedStatement ps = null;
        boolean success = false;
        
        try {
            try {
                connection = dataSource.getConnection();
                try {
                    ps = connection.prepareStatement("INSERT INTO Tokens "
                            + "(tokenId, employeeId, timeout) "
                            + "VALUES (?, ?, ?)");
                    ps.setString(tokenId, token.getTokenId());
                    ps.setInt(employeeId, token.getEmployeeId());
                    ps.setLong(timeout, token.getTimeout());
                    
                    ps.executeUpdate();
                    success = true;
                } finally {
                    if (ps != null) {
                        ps.close();
                    }
                }
            } finally {
                if (connection != null) {
                    connection.close();
                }
            }
        } catch (SQLException ex) {
            success = false;
            ex.printStackTrace();
        }
        
        return success;
    }
    
    /**
     * Deletes token.
     * @param tokenId the token id of the session
     * @return true if successfully deleted, false otherwise
     */
    public boolean removeToken(String tokenId) {
        Connection connection = null;
        PreparedStatement ps = null;
        boolean success = false;
        
        try {
            try {
                connection = dataSource.getConnection();
                try {
                    ps = connection.prepareStatement("DELETE FROM Tokens "
                            + "WHERE tokenId = ?");
                    ps.setString(1, tokenId);
                    
                    ps.executeUpdate();
                    success = true;
                } finally {
                    if (ps != null) {
                        ps.close();
                    }
                }
            } finally {
                if (connection != null) {
                    connection.close();
                }
            }
        } catch (SQLException ex) {
            success = false;
            ex.printStackTrace();
        }
        
        return success;
    }
    
    /**
     * Retrieves a token.
     * @param tokenId the token id to be retrieved
     * @return the token that was requested
     */
    public Token getTokenById(String tokenId) {
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        Token token = null;
        
        String selectSQL = "SELECT * FROM Tokens WHERE tokenId = ?";
        
        try {
            try {
                connection = dataSource.getConnection();
                try {
                    ps = connection.prepareStatement(selectSQL);
                    ps.setString(1, tokenId);
                    rs = ps.executeQuery();
                    
                    while (rs.next()) {
                        token = new Token();
                        
                        token.setTokenId(rs.getString("tokenId"));
                        token.setEmployeeId(rs.getInt("employeeId"));
                        token.setTimeout(rs.getLong("timeout"));
                    }
                    
                } finally {
                    if (ps != null) {
                        ps.close();
                    }
                }
            } finally {
                if (connection != null) {
                    connection.close();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return token;
    }
    
    /**
     * Retrieves all tokens.
     * @return a list of tokens
     */
    public ArrayList<Token> getTokenList() {
        
        Connection dbConnection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        ArrayList<Token> tokenList = new ArrayList<Token>();
        
        String selectSQL = "SELECT * FROM Tokens";
        
        try {
            try {
                dbConnection = dataSource.getConnection();
                try {
                    ps = dbConnection.prepareStatement(selectSQL);
                    rs = ps.executeQuery();
                    
                    while (rs.next()) {
                        Token token = new Token();
                        
                        token.setTokenId(rs.getString("tokenId"));
                        token.setEmployeeId(rs.getInt("employeeId"));
                        token.setTimeout(rs.getLong("timeout"));
                        
                        tokenList.add(token);
                    }
                } finally {
                    if (ps != null) {
                        ps.close();
                    }
                }
            } finally {
                if (dbConnection != null) {
                    dbConnection.close();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return tokenList;
    }
}


