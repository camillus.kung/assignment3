package com.corejsf.access;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.annotation.Resource;
import javax.enterprise.context.ConversationScoped;
import javax.sql.DataSource;

import ca.bcit.infosys.employee.Employee;
import ca.bcit.infosys.timesheet.Timesheet;
import ca.bcit.infosys.timesheet.TimesheetRow;

/**
 * DBWrapper class with CRUD operations for a Timesheet.
 * 
 * @author Rei
 * @version 1.0
 */
@ConversationScoped
public class TimesheetManager implements Serializable {

    /** The number of days in a week. */
    private static final int DAYS = 7;
    
    /** Integer value for Saturday. */
    private static final int SAT = 0;
    
    /** Integer value for Sunday. */
    private static final int SUN = 1;
    
    /** Integer value for Monday. */
    private static final int MON = 2;
    
    /** Integer value for Tuesday. */
    private static final int TUE = 3;
    
    /** Integer value for Wednesday. */
    private static final int WED = 4;
    
    /** Integer value for Thursday. */
    private static final int THU = 5;
    
    /** Integer value for Friday. */
    private static final int FRI = 6;
    
    /** dataSource for connection pool on JBoss AS 7 or higher. */
    @Resource(mappedName = "java:jboss/datasources/assignment3")
    private DataSource dataSource;
    
    /**
     * Query for retrieving all Timesheets in DB.
     * @return all Timesheets in DB.
     * @throws SQLException SQL exception.
     */
    public List<Timesheet> getAllTimesheets() throws SQLException {
        
        Connection dbConnection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        ArrayList<Timesheet> timesheetList = new ArrayList<Timesheet>();
        
        String selectSQL = "SELECT * FROM Timesheet";
        
        try {
            dbConnection = dataSource.getConnection();
            ps = dbConnection.prepareStatement(selectSQL);
            
            rs = ps.executeQuery();
        
            while (rs.next()) {
                Calendar c = new GregorianCalendar();
                
                Timesheet timesheet = new Timesheet();
                
                timesheet.setDetails(getTimesheetRowsByEmpIdAndWeekNo(
                        rs.getInt("employeeId"), rs.getInt("weekNo")));
                timesheet.setEmployee(getEmployeeByEmpId(
                        rs.getInt("employeeId")));
                timesheet.setWeekNumber(
                        rs.getInt("weekNo"), c.get(Calendar.YEAR));
                
                timesheetList.add(timesheet);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            if (ps != null) {
                ps.close();
            }

            if (dbConnection != null) {
                dbConnection.close();
            }
        }
        return timesheetList;
    }
    
    /**
     * Query for retrieving all Timesheets by an Employee ID.
     * @param employeeId employee's Id.
     * @return all Timesheets for that Employee.
     * @throws SQLException SQL exception.
     */
    public ArrayList<Timesheet> getAllTimesheetsByEmployee(int employeeId) 
            throws SQLException {
        Connection dbConnection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        ArrayList<Timesheet> timesheetList = new ArrayList<Timesheet>();
        
        String selectSQL = "SELECT * FROM Timesheet WHERE employeeId = " 
                + employeeId;
        
        try {
            dbConnection = dataSource.getConnection();
            ps = dbConnection.prepareStatement(selectSQL);
            
            rs = ps.executeQuery();
        
            while (rs.next()) {
                Calendar c = new GregorianCalendar();
                
                Timesheet timesheet = new Timesheet();

                timesheet.setDetails(getTimesheetRowsByEmpIdAndWeekNo(
                        rs.getInt("employeeId"), rs.getInt("weekNo")));
                timesheet.setEmployee(getEmployeeByEmpId(
                        rs.getInt("employeeId")));
                timesheet.setWeekNumber(
                        rs.getInt("weekNo"), c.get(Calendar.YEAR));
                
                timesheetList.add(timesheet);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            if (ps != null) {
                ps.close();
            }

            if (dbConnection != null) {
                dbConnection.close();
            }
        }
        return timesheetList;
    }

    /**
     * Query for retrieving a Timesheet by Employee ID and Week No.
     * @param empId employee's Id.
     * @param weekNo week number.
     * @return Timesheet for that employee and week number.
     * @throws SQLException SQL exception.
     */
    public Timesheet getTimesheetByEmployeeAndWeekNo(int empId, int weekNo) 
            throws SQLException {
        Connection dbConnection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        Timesheet timesheet = new Timesheet();
        
        String selectSQL = "SELECT * FROM Timesheet WHERE employeeId = " 
                + empId + " AND weekNo = " + weekNo;
        
        try {
            dbConnection = dataSource.getConnection();
            ps = dbConnection.prepareStatement(selectSQL);
            
            rs = ps.executeQuery();
        
            if (rs.next()) {
                timesheet.setDetails(getTimesheetRowsByEmpIdAndWeekNo(
                        rs.getInt("employeeId"), rs.getInt("weekNo")));
                timesheet.setEmployee(getEmployeeByEmpId(
                        rs.getInt("employeeId")));
            } else {
                timesheet = null;
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            if (ps != null) {
                ps.close();
            }

            if (dbConnection != null) {
                dbConnection.close();
            }
        }
        return timesheet;
    }
    
    /**
     * Query for retrieving Timesheet Rows for a Timesheet.
     * @param empId employee's id.
     * @param weekNo week number.
     * @return Timesheet Rows for that Timesheet.
     * @throws SQLException SQL exception.
     */
    public ArrayList<TimesheetRow> getTimesheetRowsByEmpIdAndWeekNo(
            int empId, int weekNo) 
            throws SQLException {
        
        Connection dbConnection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        ArrayList<TimesheetRow> timesheetRowList = 
                new ArrayList<TimesheetRow>();
        
        String selectSQL = "SELECT * FROM TimesheetRow WHERE employeeId = " 
                + empId + " AND weekNo = " + weekNo;
        
        try {
            dbConnection = dataSource.getConnection();
            ps = dbConnection.prepareStatement(selectSQL);
            
            rs = ps.executeQuery();
        
            while (rs.next()) {
                TimesheetRow timesheetRow = new TimesheetRow();
                
                timesheetRow.setProjectID(rs.getInt("projectId"));
                timesheetRow.setWorkPackage(rs.getString("workPackage"));
                
                BigDecimal[] hoursForWeek = new BigDecimal[DAYS];
                hoursForWeek[SAT] = BigDecimal.valueOf(rs.getDouble("Sat"));
                hoursForWeek[SUN] = BigDecimal.valueOf(rs.getDouble("Sun"));
                hoursForWeek[MON] = BigDecimal.valueOf(rs.getDouble("Mon"));
                hoursForWeek[TUE] = BigDecimal.valueOf(rs.getDouble("Tue"));
                hoursForWeek[WED] = BigDecimal.valueOf(rs.getDouble("Wed"));
                hoursForWeek[THU] = BigDecimal.valueOf(rs.getDouble("Thu"));
                hoursForWeek[FRI] = BigDecimal.valueOf(rs.getDouble("Fri"));
                
                timesheetRow.setHoursForWeek(hoursForWeek);
                timesheetRow.setNotes(rs.getString("notes"));
                
                timesheetRowList.add(timesheetRow);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            if (ps != null) {
                ps.close();
            }

            if (dbConnection != null) {
                dbConnection.close();
            }
        }
        return timesheetRowList;
    }
    
    /**
     * Query to grab an Employee by their Id.
     * @param empId the employee's Id.
     * @return the Employee object.
     */
    public Employee getEmployeeByEmpId(int empId) {
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        Employee employee = new Employee();
        
        String selectSQL = "SELECT employeeId, firstName, lastName, userName "
                + "FROM User WHERE employeeId = ?";
        
        try {
            try {
                connection = dataSource.getConnection();
                try {
                    ps = connection.prepareStatement(selectSQL);
                    ps.setInt(1, empId);
                    rs = ps.executeQuery();
                    
                    while (rs.next()) {
                        employee.setEmpNumber(rs.getInt("employeeId"));
                        employee.setName(rs.getString("firstName") + " " 
                        + rs.getString("lastName"));
                        employee.setUserName(rs.getString("userName"));
                    }
                    
                } finally {
                    if (ps != null) {
                        ps.close();
                    }
                }
            } finally {
                if (connection != null) {
                    connection.close();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return employee;
    }
    
    /**
     * Query to add a Timesheet.
     * @param timesheet the timesheet to add.
     */
    public void addTimesheet(Timesheet timesheet) {
        
        //order of fields in INSERT statement
        final int employeeId = 1;
        final int weekNo = 2;
        
        Connection connection = null;
        PreparedStatement ps = null;
        
        try {
            try {
                connection = dataSource.getConnection();
                try {
                    ps = connection.prepareStatement(
                            "INSERT INTO Timesheet (employeeId, weekNo) "
                            + "VALUES (?, ?)");
                    ps.setInt(
                            employeeId, 
                            timesheet.getEmployee().getEmpNumber());
                    ps.setInt(weekNo, timesheet.getWeekNumber());
                    
                    ps.executeUpdate();
                } finally {
                    if (ps != null) {
                        ps.close();
                    }
                }
            } finally {
                if (connection != null) {
                    connection.close();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        
        for (int i = 0; i < timesheet.getDetails().size(); i++) {
            addTimesheetRow(
                    timesheet.getEmployee().getEmpNumber(), 
                    timesheet.getWeekNumber(), 
                    timesheet.getDetails().get(i));
        }
    }
    
    /**
     * Query to add a Timesheet Row.
     * @param empId the employee's Id.
     * @param weekNo the week number
     * @param timesheetRow the Timesheet Row to add.
     */
    public void addTimesheetRow(
            int empId, int weekNo, TimesheetRow timesheetRow) {
        
        //order of fields in INSERT statement
        final int employeeId = 1;
        final int weekNumber = 2;
        final int projectId = 3;
        final int workPackage = 4;
        final int displacement = 5;
        final int notes = 12;
        
        Connection connection = null;
        PreparedStatement ps = null;
        
        try {
            try {
                connection = dataSource.getConnection();
                try {
                    ps = connection.prepareStatement(
                            "INSERT INTO TimesheetRow ("
                            + "employeeId, weekNo, projectId, workPackage, "
                            + "sat, sun, mon, tue, wed, thu, fri, notes) "
                            + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                    
                    ps.setInt(employeeId, empId);
                    ps.setInt(weekNumber, weekNo);
                    ps.setInt(projectId, timesheetRow.getProjectID());
                    ps.setString(workPackage, timesheetRow.getWorkPackage());
                    for (int i = 0; i < DAYS; i++) {
                        if (timesheetRow.getHour(i) == null) {
                            ps.setDouble(i + displacement, 0.0);
                        } else {
                            ps.setDouble(
                                    i + displacement, 
                                    timesheetRow.getHour(i).doubleValue());
                        }
                    }
                    ps.setString(notes, timesheetRow.getNotes());
                    
                    ps.executeUpdate();
                } finally {
                    if (ps != null) {
                        ps.close();
                    }
                }
            } finally {
                if (connection != null) {
                    connection.close();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     * Query to delete a Timesheet row by EmpId and WeekNo.
     * @param employeeId employee's Id.
     * @param weekNumber week number.
     */
    public void deleteTimesheetRowsByEmpIdAndWeekNo(
            int employeeId, int weekNumber) {
        
        //order of fields in DELETE statement
        final int empId = 1;
        final int weekNo = 2;
        
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            try {
                connection = dataSource.getConnection();
                try {
                    ps = connection.prepareStatement(
                            "DELETE FROM TimesheetRow "
                            + "WHERE employeeId = ? AND weekNo = ?");
                    ps.setInt(empId, employeeId);
                    ps.setInt(weekNo, weekNumber);
                    ps.executeUpdate();
                } finally {
                    if (ps != null) {
                        ps.close();
                    }
                }
            } finally {
                if (connection != null) {
                    connection.close();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     * Query to delete a Timesheet by EmpId and WeekNo.
     * @param employeeId employee's Id.
     * @param weekNumber week number.
     */
    public void deleteTimesheetByEmpIdAndWeekNo(
            int employeeId, int weekNumber) {
        deleteTimesheetRowsByEmpIdAndWeekNo(employeeId, weekNumber);
        
        //order of fields in DELETE statement
        final int empId = 1;
        final int weekNo = 2;
        
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            try {
                connection = dataSource.getConnection();
                try {
                    ps = connection.prepareStatement(
                            "DELETE FROM Timesheet "
                            + "WHERE employeeId = ? AND weekNo = ?");
                    ps.setInt(empId, employeeId);
                    ps.setInt(weekNo, weekNumber);
                    ps.executeUpdate();
                } finally {
                    if (ps != null) {
                        ps.close();
                    }
                }
            } finally {
                if (connection != null) {
                    connection.close();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     * Query to delete a Timesheet Row by EmpId.
     * @param employeeId employee's Id.
     */
    public void deleteTimesheetRowsByEmpId(int employeeId) {
        
        //order of fields in DELETE statement
        final int empId = 1;
        
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            try {
                connection = dataSource.getConnection();
                try {
                    ps = connection.prepareStatement(
                            "DELETE FROM TimesheetRow "
                            + "WHERE employeeId = ?");
                    ps.setInt(empId, employeeId);
                    ps.executeUpdate();
                } finally {
                    if (ps != null) {
                        ps.close();
                    }
                }
            } finally {
                if (connection != null) {
                    connection.close();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     * Query to delete a Timesheet by EmpId.
     * @param employeeId employee's Id.
     */
    public void deleteTimesheetByEmpId(int employeeId) {
        deleteTimesheetRowsByEmpId(employeeId);
        
        //order of fields in DELETE statement
        final int empId = 1;
        
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            try {
                connection = dataSource.getConnection();
                try {
                    ps = connection.prepareStatement(
                            "DELETE FROM Timesheet "
                            + "WHERE employeeId = ?");
                    ps.setInt(empId, employeeId);
                    ps.executeUpdate();
                } finally {
                    if (ps != null) {
                        ps.close();
                    }
                }
            } finally {
                if (connection != null) {
                    connection.close();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     * Query to update a Timesheet.
     * @param timesheet the timesheet to update.
     */
    public void updateTimesheet(Timesheet timesheet) {
        deleteTimesheetRowsByEmpIdAndWeekNo(
                timesheet.getEmployee().getEmpNumber(), 
                timesheet.getWeekNumber());
        
        for (int i = 0; i < timesheet.getDetails().size(); i++) {
            addTimesheetRow(
                    timesheet.getEmployee().getEmpNumber(), 
                    timesheet.getWeekNumber(), 
                    timesheet.getDetails().get(i));
        }
    }
    
    /**
     * Query to update a Timesheet by Employee Id.
     * @param newEmpNum employee's new Id.
     * @param oldEmpNum employee's old Id.
     */
    public void updateTimesheetByEmployee(int newEmpNum, int oldEmpNum) {
        updateTimesheetRowsByEmployee(newEmpNum, oldEmpNum);
        
        //order of fields in INSERT statement
        final int newId = 1;
        final int oldId = 2;
        
        Connection connection = null;
        PreparedStatement ps = null;
        
        try {
            try {
                connection = dataSource.getConnection();
                try {
                    ps = connection.prepareStatement(
                            "UPDATE Timesheet "
                            + "SET employeeId = ? "
                            + "WHERE employeeId = ?");
                    ps.setInt(newId, newEmpNum);
                    ps.setInt(oldId, oldEmpNum);
                    
                    ps.executeUpdate();
                } finally {
                    if (ps != null) {
                        ps.close();
                    }
                }
            } finally {
                if (connection != null) {
                    connection.close();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     * Query to update a Timesheet Row by Employee Id.
     * @param newEmpNum employee's new Id.
     * @param oldEmpNum employee's old Id.
     */
    public void updateTimesheetRowsByEmployee(int newEmpNum, int oldEmpNum) {
        //order of fields in INSERT statement
        final int newId = 1;
        final int oldId = 2;
        
        Connection connection = null;
        PreparedStatement ps = null;
        
        try {
            try {
                connection = dataSource.getConnection();
                try {
                    ps = connection.prepareStatement(
                            "UPDATE TimesheetRow "
                            + "SET employeeId = ? "
                            + "WHERE employeeId = ?");
                    ps.setInt(newId, newEmpNum);
                    ps.setInt(oldId, oldEmpNum);
                    
                    ps.executeUpdate();
                } finally {
                    if (ps != null) {
                        ps.close();
                    }
                }
            } finally {
                if (connection != null) {
                    connection.close();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
