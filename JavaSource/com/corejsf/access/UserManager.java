package com.corejsf.access;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.annotation.Resource;
import javax.enterprise.context.ConversationScoped;
import javax.sql.DataSource;

import com.corejsf.model.User;

/**
 * Resources to display users.
 * @author camilluskung
 * @version 1
 *
 */
@ConversationScoped
public class UserManager implements Serializable {
    
    /** 
     * dataSource for connection pool on JBoss AS 7 or higher. 
     */
    @Resource(mappedName = "java:jboss/datasources/assignment3")
    private DataSource dataSource;
    
    /**
     * Retrieves the list of all users in the database.
     * @return the list of users
     */
    public ArrayList<User> getUserList() {
        
        Connection dbConnection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        ArrayList<User> userList = new ArrayList<User>();
        
        String selectSQL = "SELECT * from User";
        
        try {
            try {
                dbConnection = dataSource.getConnection();
                try {
                    ps = dbConnection.prepareStatement(selectSQL);
                    rs = ps.executeQuery();
                    
                    while (rs.next()) {
                        if (rs.getInt("EmployeeId") == 0) {
                            continue;
                        }
                        User user = new User();
                        
                        user.setEmployeeId(rs.getInt("employeeId"));
                        user.setFirstName(rs.getString("firstName"));
                        user.setLastName(rs.getString("lastName"));
                        user.setUserName(rs.getString("userName"));
                        user.setPassword(rs.getString("password"));
                        
                        userList.add(user);
                    }
                } finally {
                    if (ps != null) {
                        ps.close();
                    }
                }
            } finally {
                if (dbConnection != null) {
                    dbConnection.close();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userList;
    }
    
    /**
     * Verifies the user in the system.
     * @param username The username of the user
     * @param password The password of the user
     * @return user logging in
     */
    public User verify(String username, String password) {
        
        Connection dbConnection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        User user = null;
        
        String selectSQL = "SELECT * from User WHERE userName = ? "
                + "AND password = ?";
        
        try {
            try {
                dbConnection = dataSource.getConnection();
                try {
                    ps = dbConnection.prepareStatement(selectSQL);
                    ps.setString(1, username);
                    ps.setString(2, password);
                    rs = ps.executeQuery();
                    
                    while (rs.next()) {
                        user = new User();
                        
                        user.setEmployeeId(rs.getInt("employeeId"));
                        user.setFirstName(rs.getString("firstName"));
                        user.setLastName(rs.getString("lastName"));
                        user.setUserName(rs.getString("userName"));
                        user.setPassword(rs.getString("password"));
                        
                    }
                } finally {
                    if (ps != null) {
                        ps.close();
                    }
                }
            } finally {
                if (dbConnection != null) {
                    dbConnection.close();
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return user;
    }

    /**
     * Checks to see if the user exists in the database.
     * @param employeeNumber the employee number of the user
     * @param userName the username of the user
     * @return true if user exists false otherwise
     */
    public boolean userNumberOrUsernameExists(int employeeNumber, 
            String userName) {
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        boolean exists = false;
        
        String selectSQL = "SELECT * from User WHERE employeeId = ? OR "
                + "userName = ?";
        
        try {
            try {
                connection = dataSource.getConnection();
                try {
                    ps = connection.prepareStatement(selectSQL);
                    ps.setInt(1, employeeNumber);
                    ps.setString(2, userName);
                    
                    rs = ps.executeQuery();
                    
                    if (rs.next()) {
                        exists = true;
                    }
                } finally {
                    if (ps != null) {
                        ps.close();
                    }
                }
            } finally {
                if (connection != null) {
                    connection.close();
                }
            }
        } catch (SQLException ex) {
            exists = false;
            ex.printStackTrace();
        }
        
        return exists;
    }
    
    /**
     * Adds a user to the user table.
     * @param user the user to be added
     * @return true of adding a user is successful, false otherwise
     */
    public boolean addUser(User user) {
        //order of fields in INSERT statement
        final int employeeId = 1;
        final int firstName = 2;
        final int lastName = 3;
        final int userName = 4;
        final int password = 5;
        
        Connection connection = null;
        PreparedStatement ps = null;
        boolean success = false;
        
        try {
            try {
                connection = dataSource.getConnection();
                try {
                    ps = connection.prepareStatement("INSERT INTO User "
                            + "(employeeId, firstName, lastName, userName, "
                            + "password) "
                            + "VALUES (?, ?, ?, ?, ?)");
                    ps.setInt(employeeId, user.getEmployeeId());
                    ps.setString(firstName, user.getFirstName());
                    ps.setString(lastName, user.getLastName());
                    ps.setString(userName, user.getUserName());
                    ps.setString(password, user.getPassword());
                    
                    ps.executeUpdate();
                    success = true;
                } finally {
                    if (ps != null) {
                        ps.close();
                    }
                }
            } finally {
                if (connection != null) {
                    connection.close();
                }
            }
        } catch (SQLException ex) {
            success = false;
            ex.printStackTrace();
        }
        
        return success;
    }
    
    /**
     * Updates the user in the user table.
     * @param user the user to be updated
     */
    public void updateUser(User user) {
        //order of fields in UPDATE statement
        final int firstName = 1;
        final int lastName = 2;
        final int userName = 3;
        final int password = 4;
        final int employeeId = 5;
        
        Connection connection = null;
        PreparedStatement ps = null;
        
        try {
            try {
                connection = dataSource.getConnection();
                try {
                    ps = connection.prepareStatement("UPDATE User SET "
                            + "firstName = ?, lastName = ?, userName = ?, "
                            + "password = ? WHERE employeeId = ?");
                    ps.setString(firstName, user.getFirstName());
                    ps.setString(lastName, user.getLastName());
                    ps.setString(userName, user.getUserName());
                    ps.setString(password, user.getPassword());
                    ps.setInt(employeeId, user.getEmployeeId());
                    
                    ps.executeUpdate();
                } finally {
                    if (ps != null) {
                        ps.close();
                    }
                }
            } finally {
                if (connection != null) {
                    connection.close();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
    
    /**
     * Deletes the user from the user table.
     * @param user the user to be deleted
     * @return true if user is deleted, false otherwise
     */
    public boolean deleteUser(User user) {
        Connection connection = null;
        PreparedStatement ps = null;
        boolean success = false;
        try {
            try {
                connection = dataSource.getConnection();
                try {
                    ps = connection.prepareStatement("DELETE FROM User "
                            + "WHERE employeeId = ?");
                    ps.setInt(1, user.getEmployeeId());
                    ps.executeUpdate();
                    success = true;
                } finally {
                    if (ps != null) {
                        ps.close();
                    }
                }
            } finally {
                if (connection != null) {
                    connection.close();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            success = false;
        }
        
        return success;
    }
    
    /**
     * Retrieves a user by their user id.
     * @param employeeId the employee id of the user
     * @return the user
     */
    public User getUserById(int employeeId) {
        Connection connection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        User user = null;
        
        String selectSQL = "SELECT * FROM User WHERE employeeId = ?";
        
        try {
            try {
                connection = dataSource.getConnection();
                try {
                    ps = connection.prepareStatement(selectSQL);
                    ps.setInt(1, employeeId);
                    rs = ps.executeQuery();
                    
                    while (rs.next()) {
                        user = new User();
                        
                        user.setEmployeeId(rs.getInt("employeeId"));
                        user.setFirstName(rs.getString("firstName"));
                        user.setLastName(rs.getString("lastName"));
                        user.setUserName(rs.getString("userName"));
                        user.setPassword(rs.getString("password"));
                    }
                    
                } finally {
                    if (ps != null) {
                        ps.close();
                    }
                }
            } finally {
                if (connection != null) {
                    connection.close();
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return user;
    }

}
